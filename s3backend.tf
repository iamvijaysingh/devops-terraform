terraform {
 ## This to upload tfstate file in S3, first create S3 Bucket through AWS console ###   
 backend "S3" {
 bucket = "vijay-terraform-backend"
 key = "Terraform.tfstate"
 region = "ap-south-1"
 #access_key = ""
 #secret_key = ""
  ## This to lock state file in S3, first create dynamo db table through AWS console ###
 dynamodb_table = "vijays3-state.lock"
 }
}